#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include "DatosMemCompartida.h"

int main () {

	DatosMemCompartida *memcomp;
	int bottxt;

	bottxt=open("/tmp/bot", O_RDWR);
	memcomp=(DatosMemCompartida*) mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARE, bottxt, 0);
	close(bottxt);

	while(1){
		if(memcomp->raqueta1.y1 < memcomp->esfera.centro.y) memcomp->accion=1;
		else if(memcomp->raqueta1.y2 > memcomp->esfera.centro.y) memcomp->accion=-1;
		usleep(25000);
	}

	return 0;
}
